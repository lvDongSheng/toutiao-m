/* postcss 的配置文件 */
module.exports = {
  plugins: {
    'postcss-pxtorem': {
      // 转换的根元素基准值
      rootValue: 37.5,
      // 需要转换的 CSS 属性
      propList: ['*']
    }
  }
}
