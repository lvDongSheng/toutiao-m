import Vue from 'vue'
import Vuex from 'vuex'
import { storage } from '@/utils'

Vue.use(Vuex)

const USER = 'user'

export default new Vuex.Store({
  state: {
    user: storage.getItem(USER) // 当前用户的登录数据
  },
  mutations: {
    // 改变登录对象
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    accessData (ctx, payload) {
      ctx.commit('setUser')
      storage.setItem(USER, payload)
    }
  },
  modules: {}
})
