import request from './request'

// 登录注册
async function login (userInfo) {
  const resp = await request.post('/app/v1_0/authorizations', userInfo)
  return resp
}

// 发送验证码
async function sendSms (mobile) {
  const resp = await request.get(`/app/v1_0/sms/codes/${mobile}`)
  return resp
}

export {
  login,
  sendSms
}
