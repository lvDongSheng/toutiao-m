/* 本地存储 */

function getItem (name) {
  const data = localStorage.getItem(name)
  try {
    return JSON.parse(data)
  } catch (err) {
    return data
  }
}

function setItem (name, value) {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
    return localStorage.setItem(name, value)
  } else {
    return localStorage.setItem(name, value)
  }
}

function removeItem (name) {
  return localStorage.removeItem(name)
}

export default {
  getItem,
  setItem,
  removeItem
}
